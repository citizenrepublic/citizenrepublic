$(document).ready(function() {
	var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;	

	// LOAD HOME AFTER LOADING THE SPLIT IMG
	$('#split-load').load( function() {
		$('.delayed-show').removeClass('delayed-show');
		$('#loader').addClass('hidden');
	});
	
	
	// SOFT SCROLL
	
	$('.softscroll').on('click', function(e) {
	   // prevent default anchor click behavior
	   e.preventDefault();
	
	   // animate
	   $('html, body').animate({
		   scrollTop: $(this.hash).offset().top - 100
	   }, 600);
	});

	
	// DISABLE AUTO PLAY
	
	$('#carousel-portfolio').carousel({
	    interval: false
	});
	
	
	// PORTFOLIO TRIGGER
	// on
	$('.portfolio').on('click', function() {
		$('.zoom').addClass('active');
		$('.zoom .item').removeClass('active');
		target = $(this).data('target'); 
		$('.zoom #' + target).addClass('active');
	});
	// off	
	$('#zoom-close').on('click', function() {
		$('.zoom').removeClass('active');
	});

	
	// SET HOME HEIGHT TO 100% OF THE WINDOW
	
	if ( x > 991) {
		$('.home-section').css('height', y + 'px');
	}

	
	// SPY SCROLL TO TRIGGER ANIMATIONS
	
	$(window).scroll(function() {
		var position = $(document).scrollTop();
		$( '.toAnimate' ).each(function() {
			var top = $(this).offset().top;
			var action = $(this).data('action');
			var offset = $(this).data('offset');
			if (top <= (position + y - offset)) {
				$(this).addClass('animated ' + action);
			}
		});
	});

	
	// LOAD IFRAME ON CLICK
	
	$('.loadIframe').on('click', function() {
		var container = $(this).data('iframecontainer');
		$( container + ' iframe' ).each(function() {
			var src = $(this).attr('src');
			if (src == '') {
				var url = $(this).data('url');
				$(this).attr('src',url);
			}
		});
	});

	
	// EMAIL FORM ERRORS
	
	$('#name, #email, #message').on('input', function() {
		$('#' + $(this).attr('id') + ' + div[class="alert alert-danger fz08"]').remove();
	});
	
	
	// EMAIL FORM SENDING
	
	$('#send-mail').on('click', function() {
		
		$('#send-mail').children().replaceWith('<i class="fa fa-spinner fa-pulse"></i>');
		
		var name = $('#name').val();
		var company = $('#company').val();
		var email = $('#email').val();
		var phone = $('#phone').val();
		var message = $('#message').val()
		
		var hasError = false;
		
		if(name.length == 0) { hasError = true; showErrorMessage('name');}
		if(email.length == 0) { hasError = true; showErrorMessage('email');}
		if(message.length == 0) { hasError = true; showErrorMessage('message');}
		
		if(hasError) {
			$('#send-mail').children().replaceWith('<i class="fa fa-chevron-right"></i>');
			return false;
		}
		
		var text = "Nom : "+name+"\nSociété : "+company+"\nEmail : "+email+"\nTéléphone : "+phone+"\nMessage : "+message;
		
		var xhr = $.ajax({
			//type: 'POST',
		    //url: 'https://mandrillapp.com/api/1.0/messages/send.json',
			type: 'GET',
			url: 'http://jsonplaceholder.typicode.com/posts/1',
		    dataType: 'json',
		    data: {
		    	key: 'arj5rMhbtIxaWuorXPRBSQ',
		    	message: {
		    		text: text,
		    		subject: 'Mail de contact depuis citizenrepublic.com',
		    		from_email: 'technique@citizenrepublic.com',
		    		from_name: 'citizenrepublic.com',
		    		to: [{
		    			'email': 'quentin.foure@citizenrepublic.com'
		    		}]
		    	}
		    }
		});

		xhr.done(function(data) {
			$('#contact-form').css('opacity', '0');
			$('<div class="well"><h3 class="arapey black">Merci, votre message a bien été envoyé.</h2></div>').insertBefore('#contact-form');
	    });
	    
		xhr.fail(function(jqXHR, textStatus, errorThrown) {
	    	$('#contact-form').css('opacity', '0');
	    	$('<div class="well"><h3 class="arapey black">Malheureusement, une erreur s\'est produite. Nous vous invitons à réessayer plus tard.</h2></div>').insertBefore('#contact-form');
	    });
	});
	
	function showErrorMessage(id) {
		
		if($('#' + id + ' + div[class="alert alert-danger fz08"]').length == 0) {
			$('<div class="alert alert-danger fz08">Vous devez compléter le champs ci-dessus</div>').insertAfter($('#'+id));
		}
	}
});
	