<!DOCTYPE html>
<html lang="fr">
	<head>
		
		<?php include './fragments/head.php'; ?>
		
<!-- 		TODO  -->
		<meta name="description" content="Description">
		<meta name="keywords" content="mots clefs">
		<meta name="author" content="Citizen Republic">
		<title></title>
		
	</head>
	<body class="home" data-spy="scroll" data-target="#main-menu" data-offset="120">
		<?php include './fragments/nav.php'; ?>
			<img src="./img/pictures/cover_split_lite.png" class="hidden" id="split-load">
			<div class="p-big m-big home-section text-center white delayed-show t1s" id="home">
				<div class="split delayed-show t1s"></div>
				<div class="spacer10"></div>
				<img class="logo animated fadeInDown" src="./img/pictures/logo_citizenrepublic_text.png">
				<div class="spacer10"></div>
				<div class="home-quote delayed-show t1s m-medium">
					<p class="h4 spaced arapey m-small-bottom"><span class="quote-icon"><img src="./img/pictos/quote_pink_left.png"></span>
					Pour ce qui est de l'avenir, il ne s'agit pas de le prévoir mais de le rendre possible.
					<span class="quote-icon"><img src="./img/pictos/quote_pink_right.png"></span></p>
					<p class="h4 lobster pink"><i class="fz09">- Antoine de Saint-Exupéry -</i></p>
				</div>
				<div class="text-center" id="loader">
					<i class="fa fa-spinner fa-spin pink fa-5x"></i>
				</div>
				<a href="#presentation" class="softscroll home-bullet delayed-show t1s"><img src="./img/pictos/bullet_next.png"></a>
			</div>
			<div class="section p-medium presentation" id="presentation">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-6 toAnimate" data-action="fadeInLeft" data-offset="-10">
							<div class="row">
								<div class="col-xs-12 col-md-11">
									<h1 class="h2 text-right">Citizen<br><span class="relative">Republic<img class="cut-slash" src="./img/pictos/cut_slash_right.png"></span></h1>
									<p class="text-right p-small m-small"><span class="hyphen"></span></p>
									<p class="m-medium-bottom">Les nouveaux modèles digitaux et les réseaux sociaux changent chaque jour les relations 
									entre les citoyens, les marques, les associations et les institutions. L’ambition de <span class="pink">Citizen 
									Republic</span> est de vous permettre d’engager chaque jour, un nouveau dialogue vivant et constructif 
									avec l’ensemble de vos publics pour mieux les connaitre, les comprendre, et partager leurs attentes…</p>
									<p class="m-medium-bottom"><i class="fz09">Poser par ce dialogue, les bases de leur adhésion à vos valeurs et à vos objectifs, pour ainsi, les 
									inviter, les encourager à s’engager positivement et durablement à vos côtés. Cette ambition est modeste, 
									mais notre savoir-faire peut être décisif pour y parvenir.</i></p>
									<div class="row">
										<div class="col-xs-12 col-sm-7 col-sm-push-5 text-center">
											<span class="pointer loadIframe" data-toggle="modal" data-target="#videoCZR" data-iframecontainer="#videoCZR">
												<img src="./img/pictures/video_citizenrepublic.jpg" class="bordered-pink">
											</span>
										</div>		
										<div class="col-xs-12 col-sm-5 col-sm-pull-7 text-right p-small-top">
											<p class="montserrat"><span class="bold fz08 uppercase">Découvrez l'esprit</span><br>
											<span class="pink fz08 uppercase">Citizen Republic</span></p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 toAnimate" data-action="fadeInRight" data-offset="-10">
							<div class="row">
								<div class="col-xs-12 col-md-11 col-md-offset-1">
									<hr class="visible-xs-block visible-sm-block m-medium-bottom" />
									<h2>Notre<br><span class="relative">approche<img class="cut-slash left" src="./img/pictos/cut_slash_left.png"></span></h2>
									<p class="p-small m-small"><span class="hyphen"></span></p>
									<p class="m-medium-bottom">Pour irriguer les marques et les organisations, sans censure, avec respect, dans un monde ouvert et transparent…
									<br><span class="pink">Citizen Republic</span> donne le pouvoir aux citoyens, aux donateurs, aux consommateurs ou aux usagers de s’exprimer 
									et de se mobiliser à travers son programme référent d’échange d’avis et d’opinion&nbsp;: 
									<a href="https://www.monaviscompte.fr" target="_blank" class="pink">monaviscompte.fr</a>
									</p>
									<p><i class="fz09">Ce programme, fort d’une puissante communauté digitale de plus de 1&nbsp;000&nbsp;000 membres, facilite l’échange 
									entre les marque et ses publics.</i></p>
									<p class="m-medium-bottom"><i class="fz09">
									Ce programme nous permet de capter ce bouche à oreille vital pour la marque, de valoriser les bonnes idées 
									et de poser les bases d’une communication positive, durable et efficace.</i></p>
									<div class="row">
										<div class="col-xs-12 col-sm-7 text-center">
											<a href="https://www.monaviscompte.fr" target="_blank"><img src="./img/pictures/site_monaviscompte.jpg"  class="bordered-blue"></a>
										</div>
										<div class="col-xs-12 col-sm-5 p-small-top">
											<p class="montserrat"><span class="bold fz08 uppercase">Découvrez le site</span><br>
											<a href="https://www.monaviscompte.fr" target="_blank" class="pink fz08">monaviscompte.fr</a></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="section p-medium text-center bckgd-lightgrey-s black" id="quote1">
				<div class="container">
					<div class="col-xs-12">
						<p class="h4 spaced arapey m-small-bottom"><span class="quote-icon"><img src="./img/pictos/quote_pink_left.png"></span>Dans la vie, il y a 
						deux catégories d’individus&nbsp;: <br class="">ceux qui regardent le monde tel qu’il est et se demandent pourquoi.
						<br>Ceux qui imaginent le monde tel qu’il devrait être et qui se disent&nbsp;: pourquoi pas&nbsp;?
						<span class="quote-icon"><img src="./img/pictos/quote_pink_right.png"></span></p>
						<p class="h4 lobster pink"><i class="fz09">- Georges-Bernard Shaw -</i></p>
					</div>
				</div>
			</div>
			<div class="section bckgd-black-s references loadIframe" id="references" data-iframeContainer=".references">
				<div class="container text-center">
					<div class="row relative">
						<div class="col-xs-6 col-sm-6 col-md-4 p-no">
							<a class="portfolio softscroll" href="#zoom" data-offset-scroll="120" data-target="slide1" style="background-image:url(./img/pictures/portfolio/applications.jpg);">
								<div class="mask"></div>
								<div class="content">
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
									<p class="spaced lobster fz12">Le mobile solidaire</p>
									<p class="arapey pink spaced"><i class="fz08">Applications pour associations</i></p>
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
								</div>
							</a>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 p-no">
							<a class="portfolio softscroll" href="#zoom" data-offset-scroll="120" data-target="slide2" style="background-image:url(./img/pictures/portfolio/mixite.jpg);">
								<div class="mask"></div>
								<div class="content">
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
									<p class="spaced lobster fz12">Mixité des métiers</p>
									<p class="arapey pink spaced"><i>Campagne Nationale</i></p>
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
								</div>
							</a>
						</div>
						<div class="col-xs-12 visible-sm"></div>
						<div class="col-xs-6 col-sm-6 col-md-4 p-no">
							<a class="portfolio softscroll" href="#zoom" data-offset-scroll="120" data-target="slide3" style="background-image:url(./img/pictures/portfolio/aajaa.jpg);">
								<div class="mask"></div>
								<div class="content">
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
									<p class="spaced lobster fz12">aajaa</p>
									<p class="arapey pink spaced"><i class="fz08">Collaboration CHU Grenoble</i></p>
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
								</div>
							</a>
						</div>
						<div class="col-xs-12 visible-md visible-lg"></div>
						<div class="col-xs-6 col-sm-6 col-md-4 p-no">
							<a class="portfolio softscroll" href="#zoom" data-offset-scroll="120" data-target="slide4" style="background-image:url(./img/pictures/portfolio/clarins.jpg);">
								<div class="mask"></div>
								<div class="content">
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
									<p class="spaced lobster fz12">Clarins</p>
									<p class="arapey pink spaced"><i>Application iPad</i></p>
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
								</div>
							</a>
						</div>
						<div class="col-xs-12 visible-sm"></div>
						<div class="col-xs-6 col-sm-6 col-md-4 p-no">
							<a class="portfolio softscroll" href="#zoom" data-offset-scroll="120" data-target="slide5" style="background-image:url(./img/pictures/portfolio/monaviscompte.jpg);">
								<div class="mask"></div>
								<div class="content">
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
									<p class="spaced lobster fz12">monaviscompte</p>
									<p class="arapey pink spaced"><i class="fz08">Comunauté de consommateurs</i></p>
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
								</div>
							</a>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 p-no">
							<a class="portfolio softscroll" href="#zoom" data-offset-scroll="520" data-target="slide6" style="background-image:url(./img/pictures/portfolio/100_ans_de_combat.jpg);">
								<div class="mask"></div>
								<div class="content">
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
									<p class="spaced lobster fz12">100 ans de combat</p>
									<p class="arapey pink spaced"><i class="fz08">pour la liberté des femmes</i></p>
									<p class="text-center m-small"><span class="hyphen-smaller"></span></p>
								</div>
							</a>
						</div>
						<div class="anchor-zoom" id="zoom"></div>
						<div class="zoom">
							<div class="col-xs-12 relative">
								<div class="zoom-close">
									<span class="pointer btn-custom" id="zoom-close">Fermer</span>
								</div>
								<div class="carousel-controls p-small-top">
									<div class="col-xs-6 col-sm-2 col-sm-offset-2">
										<a class="" href="#carousel-portfolio" role="button" data-slide="prev">
											<img src="./img/pictos/arrow_left.jpg">
										</a>
									</div>
									<div class="col-xs-6 col-sm-2 col-sm-offset-4">
										<a class="" href="#carousel-portfolio" role="button" data-slide="next">
											<img src="./img/pictos/arrow_right.jpg">
										</a>
									</div>
								</div>
								<div id="carousel-portfolio" class="carousel slide p-medium-bottom" data-ride="carousel" data-interval="false">
									<!-- Wrapper for slides -->
									<div class="carousel-inner" role="listbox">
										<div class="item active" id="slide1">
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
													<h2><span class="fz08">Le mobile<br><span class="relative">
													solidaire<img class="cut-slash" src="./img/pictos/cut_slash_right.png"></span></span></h2>
												</div>
											</div>
											<div class="row p-small-top scroll-zoom-content">
												<div class="col-xs-12">
													<p class="text-center p-small-bottom">Les applications mobiles pour associations et fondations.</p>
												</div>
												<div class="col-xs-12 col-sm-4">
													<div class="row">
														<div class="col-xs-12 col-sm-10 col-sm-offset-2">
															<p class="text-center">
																<img src="./img/pictures/portfolio/zoom/ckdo.png">
															</p>
															<p class="uppercase montserrat bold text-left">C'Kdo</p>
															<p class="text-left"><span class="hyphen pink"></span></p>
															<p class="text-justify fz08">
																Une application développée par l'église Catholique à Paris pour gérer 
																simplement ses idées et son budget cadeaux à l'approche de Noël. Et pour 
																ceux qui le veulent, de faire un cadeau à leur paroisse (en donnant au Denier de l'Église).
															</p>
															<p>
																<a href="https://itunes.apple.com/fr/app/ckdo/id767484709">
																	<img src="./img/pictos/available-on-the-app-store.png">
																</a>
																<a href="https://play.google.com/store/apps/details?id=com.butterfly.diocese">
																	<img src="./img/pictos/available-on-the-play-store.png">
																</a>
															</p>
														</div>				
													</div>
												</div>
												<div class="col-xs-12 col-sm-4">
													<div class="row">
														<div class="col-xs-12 col-sm-10 col-sm-offset-1">
															<p class="text-center">
																<img src="./img/pictures/portfolio/zoom/cardio_info.png">
															</p>
															<p class="uppercase montserrat bold text-left">Cardio Info</p>
															<p class="text-left"><span class="hyphen pink"></span></p>
															<p class="text-justify fz08 bold">
																L’application mobile Android et iPhone Cardio Info propose un système de don innovant&nbsp;: 
																pour chaque kilomètre parcouru, un partenaire s’engage à reverser 1€ à l’association.
															</p>
															<p>
																<a href="https://itunes.apple.com/app/cardio-info/id576329230">
																	<img src="./img/pictos/available-on-the-app-store.png">
																</a>
																<a href="https://play.google.com/store/apps/details?id=com.fedecardio.app">
																	<img src="./img/pictos/available-on-the-play-store.png">
																</a>
															</p>
														</div>				
													</div>
												</div>
												<div class="col-xs-12 col-sm-4">
													<div class="row">
														<div class="col-xs-12 col-sm-10">
															<p class="text-center">
																<img src="./img/pictures/portfolio/zoom/mobile_for_good.png">
															</p>
															<p class="uppercase montserrat bold text-left">Mobile for good</p>
															<p class="text-left"><span class="hyphen pink"></span></p>
															<p class="text-justify fz08">
																Mobile for good est l'application de toutes les associations, fondations et autres organismes à but non lucratif. 
																Retrouvez près de 800 associations françaises, directement dans votre poche&nbsp;!
															</p>
															<p>
																<a href="https://itunes.apple.com/fr/app/mobile-for-good/id619910193">
																	<img src="./img/pictos/available-on-the-app-store.png">
																</a>
																<a href="https://play.google.com/store/apps/details?id=com.butterflyeffect.mobileforgood">
																	<img src="./img/pictos/available-on-the-play-store.png">
																</a>
															</p>
														</div>				
													</div>
													
												</div>
											</div>
										</div>
										<div class="item" id="slide2">
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
													<h2><span class="fz08">L'intégrale de<br><span class="relative">
													la campagne<img class="cut-slash" src="./img/pictos/cut_slash_right.png"></span></span></h2>
												</div>
											</div>
											<div class="row p-small-top scroll-zoom-content">
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-7 col-sm-offset-4">
															<p class="uppercase montserrat bold text-left">Le film <span class="pink">TV</span></p>
 															<iframe title="YouTube video player" width="100%" height="160" src=""
															frameborder="0" allowfullscreen="" data-url="http://www.youtube.com/embed/p-y-VYZ073s?rel=0"></iframe>
															<p class="p-medium-top p-small-bottom text-left"><span class="hyphen pink"></span></p>
															<p class="uppercase montserrat bold text-left">Le soutien du <span class="pink">SNPTV</span></p>
															<a href="./img/pictures/portfolio/SNPTV_1024x673.jpg"><img src="./img/pictures/portfolio/snptv_support.jpg"></a>
														</div>				
													</div>
													
												</div>
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-7 col-sm-offset-1">
															<p class="uppercase montserrat bold text-left">La soirée de <span class="pink">Lancement</span></p>
															<iframe title="YouTube video player" width="100%" height="160" src="" 
															frameborder="0" allowfullscreen="" data-url="http://www.youtube.com/embed/6HOgML8a3hI?rel=0"></iframe>
															<p class="p-medium-top p-small-bottom text-left"><span class="hyphen pink"></span></p>
															<p class="uppercase montserrat bold text-left">Dossier <span class="pink">Presse</span></p>
															<div class="row">
																<div class="col-xs-6">
																	<img src="./img/pictures/portfolio/dossier_presse_snptv.jpg">
																</div>
																<div class="col-xs-6">
																	<a href="" class="black fz08 montserrat bold uppercase"><img src="./img/pictos/download_rounded.png">
																	<br><br><span>télécharger</span></a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="item" id="slide3">
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
													<h2><span class="fz08">Application<br><span class="relative">
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AAJAA<img class="cut-slash" src="./img/pictos/cut_slash_right.png"></span></span></h2>
												</div>
											</div>
											<div class="row p-medium-top scroll-zoom-content">
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-8 col-sm-offset-4">
															<p class="uppercase montserrat bold text-left">Une application mobile en collaboration avec le CHU de Grenoble</p>
															<p class="p-small-bottom text-left"><span class="hyphen pink"></span></p>
															<p class="text-justify fz08">
																Cette application a pour objectif de faciliter l'accompagnement et le suivi d'un programme personnalisé 
																d'activité physique adaptée. Elle permet au patient de maintenir un suivi de ses activités et objectifs tout 
																en étant en dehors de l'hôpital. Elle est aussi un moyen simple de dialoguer avec l'accompagnateur pour définir 
																le programme physique le plus adapté.
															</p>
															<p class="text-justify fz08">
																L'application est aussi un outil de recherche pour permettre de mesurer l'impact de la pratique d'activité physique adaptée 
																sur la vie des patients.
															</p>
														</div>				
													</div>
													
												</div>
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-8 col-sm-offset-1">
															<p class="text-center">
																<img src="./img/pictures/portfolio/zoom/aajaa.png">
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="item" id="slide4">
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
													<h2><span class="fz08">Application<br><span class="relative">
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clarins<img class="cut-slash" src="./img/pictos/cut_slash_right.png"></span></span></h2>
												</div>
											</div>
											<div class="row p-medium-top scroll-zoom-content">
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-7 col-sm-offset-4 p-medium-top">
															<p class="uppercase montserrat bold text-left">Application iPad de street marketing</p>
															<p class="p-small-bottom text-left"><span class="hyphen pink"></span></p>
															<p class="text-justify m-small-top">Utilisée par des hôtesses l'application offrait un support pour la présentation du 
															produit aux passants et permettait la collecte d'information de contact avant d'orienter 
															l'intéressé vers un magasin partenaire.</p>
														</div>				
													</div>
													
												</div>
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-7 col-sm-offset-1">
															<p class="text-center p-medium-top">
																<img src="./img/pictures/portfolio/zoom/clarins.jpg">
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="item" id="slide5">
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
													<h2><span class="fz08">Plateforme<br><span class="relative">
													monaviscompte.fr<img class="cut-slash" src="./img/pictos/cut_slash_right.png"></span></span></h2>
												</div>
											</div>
											<div class="row p-medium-top scroll-zoom-content">
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-7 col-sm-offset-4">
															<p class="text-center p-medium-top">
																<img src="./img/pictures/portfolio/zoom/imac_monaviscompte.jpg">
															</p>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-7 col-sm-offset-1 p-medium-top">
															<p class="uppercase montserrat bold text-left">Site communautaire d'avis consommateurs</p>
															<p class="p-small-bottom text-left"><span class="hyphen pink"></span></p>
															<p class="text-justify m-small-top">Le site monaviscompte.fr et l'outil principal de Citizen Republic pour ce qui concerne&nbsp;: 
															les avis consommateurs, les études et analyses des tendances et des notoriétés, la vigie marketing.</p>
															<p class="text-center p-medium-top"><a class="btn-custom" target="_blank" href="https://monaviscompte.fr">Voir le site</a></p>
														</div>				
													</div>
												</div>
											</div>
										</div>
										<div class="item" id="slide6">
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
													<h2><span class="fz08">100 ans<br><span class="relative">
													de combats<img class="cut-slash" src="./img/pictos/cut_slash_right.png"></span></span></h2>
												</div>
											</div>
											<div class="row p-medium-top scroll-zoom-content">
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-7 col-sm-offset-4">
															<p class="uppercase montserrat bold text-left">La question de l'égalité entre les femmes et les hommes est plus que jamais au coeur de l'actualité.</p>
															<p class="p-small-bottom text-left"><span class="hyphen pink"></span></p>
															<p class="text-justify m-small-top">
																Pour mieux cerner, combattre, réduire les inégalités femmes-hommes que la quasi-totalité des Français trouvent choquantes et absurdes, 
																Frédérique Agnès a rédigé ce livre qui relate le chemin parcouru en cent ans de progrès.
															</p>
														</div>				
													</div>
													
												</div>
												<div class="col-xs-12 col-sm-6">
													<div class="row">
														<div class="col-xs-12 col-sm-7 col-sm-offset-1">
															<p class="text-center p-medium-top">
																<img src="./img/pictures/portfolio/zoom/100_ans.jpg">
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="section p-medium metiers" id="metiers">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<h2>Nos<br><span class="relative">métiers<img class="cut-slash" src="./img/pictos/cut_slash_right.png"></span></h2>
							<p class="p-medium-top m-small-bottom m-medium-top"><span class="hyphen"></span></p>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-8">
							<p class="m-small-bottom">Après plus de 25 ans au service des stratégies des principaux annonceurs commerciaux, institutionnels ou associatifs, 
							nous nous sommes fixés comme mission de réconcilier ceux qui regardent le monde tel qu’il est et, ceux qui imaginent 
							le monde tel qu’il devrait être.</p>
							<p><i>Grâce à notre capacité à écouter et animer cette communauté, nous avons bâti une méthode unique qui vous donne une 
							nouvelle lecture de votre marché, une nouvelle approche pour créer des campagnes impactantes, bâtir des stratégies originales, 
							mobiliser et lever des fonds pour vos projets, encourager de nouveaux comportements, disposer d’une source inépuisable 
							d’inspiration pour le développement de contenus à destination de vos publics.</i></p>
						</div>
					</div>
					<div class="row m-medium-bottom">
						<div class="col-xs-12 col-md-8 col-md-offset-4 text-center toAnimate" data-action="fadeInUp" data-offset="-100">
							<div class="row">
								<div class="col-xs-12 col-sm-4">
									<img src="./img/pictos/institute.png" width="120px">
									<p class="black uppercase fz12 bold montserrat m-no">L'institut</p>
									<p class="pink arapey m-no"><i>Comprendre et Partager</i></p>
									<p class="text-center"><span class="hyphen-smaller"></span></p>
									<p>(&nbsp;<a href="https://www.monaviscompte.fr" target="_blank">
									<img src="./img/pictures/logo_monaviscompte.jpg"></a>&nbsp;)</p>
									<p>Avis consommateurs<br>Etudes et analyses<br>Vigie Marketing</p>
								</div>
								<div class="col-xs-12 col-sm-4">
									<img src="./img/pictos/agency.png" width="120px">
									<p class="black uppercase fz12 bold montserrat m-no">L'agence</p>
									<p class="pink arapey m-no"><i>Positionner et Communiquer</i><span class="fz12">&nbsp;</span></p>
									<p class="text-center"><span class="hyphen-smaller"></span></p>
									<p><br>Marketing stratégique<br>Mobilisation et collecte de fonds<br>Communication corporate</p>
								</div>
								<div class="col-xs-12 col-sm-4">
									<img src="./img/pictos/content.png" width="120px">
									<p class="black uppercase fz12 bold montserrat m-no">Les contenus</p>
									<p class="pink arapey m-no"><i>Animer et Engager</i><span class="fz12">&nbsp;</span></p>
									<p class="text-center"><span class="hyphen-smaller"></span></p>
									<p><br>E-réputation<br>Vidéo marketing<br>Marketing communautaire</p>
								</div>
							</div>	
						</div>	
					</div>
				</div>
			</div>
			<div class="tools-section bckgd-lightgrey-s p-medium" id="outils">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-4 toAnimate" data-action="fadeInLeft" data-offset="-10">
							<h2>Notre<br>Boîte <span class="pink">à<br>outils</span></h2>
							<p>Afin d’accompagner nos annonceurs,<br>de rendre possible leurs ambitions et<br>leur vision, nous agissons</p>
						</div>
						<div class="col-xs-12 col-sm-8 toAnimate" data-action="fadeInRight" data-offset="-10">
							<div class="row  p-small">
								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0">
									<div class="row">
										<div class="col-xs-3">
											<img src="./img/pictos/doc.png" width="50px">
										</div>
										<div class="col-xs-9">
											<p class="black uppercase bold montserrat spaced spaced">Planning<br>Stratégique</p>
										</div>		
									</div>
								</div>
								<div class="col-xs-12 p-small visible-xs"></div>
								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0">
									<div class="row">
										<div class="col-xs-3">
											<img src="./img/pictos/persons.png" width="50px">
										</div>
										<div class="col-xs-9">
											<p class="black uppercase bold montserrat spaced spaced">Accompagnement<br>au changement</p>
										</div>		
									</div>
								</div>
							</div>
							<div class="row  p-small">
								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0">
									<div class="row">
										<div class="col-xs-3">
											<img src="./img/pictos/star.png" width="50px">	
										</div>
										<div class="col-xs-9">
											<p class="black uppercase bold montserrat spaced spaced">Stratégie<br>de communication</p>
										</div>		
									</div>
								</div>
								<div class="col-xs-12 p-small visible-xs"></div>
								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0">
									<div class="row">
										<div class="col-xs-3">
											<img src="./img/pictos/computer.png" width="50px">
										</div>
										<div class="col-xs-9">
											<p class="black uppercase bold montserrat spaced spaced">Stratégie<br>digitale</p>
										</div>		
									</div>
								</div>
							</div>
							<div class="row  p-small">
								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0">
									<div class="row">
										<div class="col-xs-3">
											<img src="./img/pictos/share.png" width="50px">
										</div>
										<div class="col-xs-9">
											<p class="black uppercase bold montserrat spaced spaced">Acquisition<br>et fidélisation</p>
										</div>		
									</div>
								</div>
								<div class="col-xs-12 p-small visible-xs"></div>
								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0">
									<div class="row">
										<div class="col-xs-3">
											<img src="./img/pictos/finger.png" width="50px">
										</div>
										<div class="col-xs-9">
											<p class="black uppercase bold montserrat spaced spaced">Marketing<br>communautaire</p>
										</div>		
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="section p-medium equipe" id="equipe">
				<div class="container">
					<div class="row text-center">
						<h2><span class="fz12">Notre équipe</span></h2>
						<p class="text-center p-small m-small"><span class="hyphen"></span></p>
						<p class="m-medium-bottom">Au-delà de la connaissance intime de vos publics, notre plus-value repose<br>sur la qualité et la 
						complémentarité de nos expertises.</p>
						<div class="col-xs-12 col-sm-6 col-md-4 toAnimate" data-action="fadeInUp" data-offset="-50">
							<img src="./img/pictures/frederique.png" width="170px">
							<p class="m-small-top black uppercase bold montserrat spaced">Frédérique Agnès</p>
							<img src="./img/pictos/slash.png">
							<p><span class="fz09">Communication corporate<br>Marketing stratégique<br>Business intelligence<br>Mobilisation et influence</span></p>
							<img src="./img/pictos/slash2.png">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 toAnimate" data-action="fadeInUp" data-offset="-50">
							<img src="./img/pictures/farid.png" width="170px">
							<p class="m-small-top black uppercase bold montserrat spaced">Farid Akani</p>
							<img src="./img/pictos/slash.png">
							<p><span class="fz09">Animation communautaire<br>Marketing stratégique<br>Communication 360°<br>Stratégie internationale</span></p>
							<img src="./img/pictos/slash2.png">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 toAnimate" data-action="fadeInUp" data-offset="-50">
							<img src="./img/pictures/eric.png" width="170px">
							<p class="m-small-top black uppercase bold montserrat spaced">Éric Depoorter</p>
							<img src="./img/pictos/slash.png">
							<p><span class="fz09">Marketing stratégique<br>Levée de fonds<br>Stratégie digitale<br>Mobilisation et influence</span></p>
							<img src="./img/pictos/slash2.png">
						</div>
					</div>
				</div>
			</div>
			<div class="section p-medium bckgd-lightgrey-s text-center" id="quote2">
				<div class="container">
					<div class="col-xs-12 col-md-10 col-md-offset-1">
						<p class="h4 spaced arapey black m-small-bottom"><span class="quote-icon"><img src="./img/pictos/quote_pink_left.png"></span>Rien de 
						grand ne se fit jamais sans enthousiasme<span class="quote-icon"><img src="./img/pictos/quote_pink_right.png"></span></p>
						<p class="h4 lobster pink"><i class="fz09">- Ralph Waldo Emerson -</i></p>
					</div>
				</div>
			</div>
			<div class="contact-section bordered-grey-top map-background relative" id="contact">
<!-- 				<div id="map" class="map""></div> -->
				<div class="container">
					<div class="row text-center">
						<div class="col-xs-12 col-md-6 toAnimate" data-action="fadeInLeft" data-offset="0">
							<div class="bckgd-white-s p-small-all m-medium box-shadowed">
								<p class="arapey pink h3">Notre approche vous intéresse&nbsp;?</p>
								<p class="arapey uppercase black h2 p-medium-bottom"><span class="fz08">Contactez-nous</span></p>
								<form class="form-light" id="contact-form">
									<div class="form-group second-name">
										<label for="name" class="sr-only">Ne rien mettre</label>
										<input type="text" class="form-control second-name" id="second-name" placeholder="Ne rien mettre">
									</div>
									<div class="form-group">
										<label for="name" class="sr-only">Nom / Prénom*</label>
										<input type="text" class="form-control" id="name" placeholder="Nom / Prénom*">
									</div>
									<div class="form-group">
										<label for="company" class="sr-only">Société</label>
										<input type="text" class="form-control" id="company" placeholder="Société">
									</div>
									<div class="form-group">
										<label for="email" class="sr-only">Email*</label>
										<input type="email" class="form-control" id="email" placeholder="Email*">
									</div>
									<div class="form-group">
										<label for="phone" class="sr-only">Téléphone</label>
										<input type="tel" class="form-control" id="phone" placeholder="Téléphone">
									</div>
									<div class="form-group">
										<label for="message" class="sr-only">Message*</label>
										<textarea rows="6" class="form-control" id="message" placeholder="Message*"></textarea>
									</div>
									<p class="text-right p-small">
										<span class="btn-custom big-pink pointer" id="send-mail">Envoyer&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></span>  
									</p>
								</form>
							</div>
						</div>
						<div class="col-xs-12 col-md-4 col-md-offset-2 toAnimate" data-action="fadeInRight" data-offset="0">
							<div class="white p-small-all m-medium relative">
								<div class="skew-shape bckgd-pink-s box-shadowed"></div>
								<div class="z-top">
									<p class="uppercase fz15 bold">Citizen Republic</p>
									<p>171 bis, Avenue Charles de Gaulle<br>92200 Neuilly sur seine</p>
									<p class="text-center"><a class="black fz08" target="_blank"
									href="https://www.google.fr/maps/place/Citizen+Republic/@48.8836123,2.2630663,17z/data=!3m1!4b1!4m2!3m1!1s0x47e6656eafefebbf:0x3a1122cda7d6e196">
									Voir sur Google Maps</a></p>
								</div>
							</div>
							<div class="p-small-all m-medium relative text-left p-medium-top">
								<div class="skew-shape bckgd-white-s box-shadowed"></div>
								<div class="row m-no">
									<div class="z-top col-xs-12 col-sm-offset-11 col-sm-offset-1">
										<p class="black uppercase fz09 bold montserrat">Frédérique AGNÈS</p>
										<p class="p-no"><span class="hyphen-smaller pink"></span></p>
										<p class="m-medium-bottom fz09">frederique.agnes@citizenrepublic.com<br>
										06 09 62 52 64</p>
										<p class="black uppercase fz09 bold montserrat">Éric DEPOORTER</p>
										<p class="p-no"><span class="hyphen-smaller pink"></span></p>
										<p class="fz09">eric.depoorter@citizenrepublic.com<br>
										06 07 40 42 79</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="section p-medium text-center bckgd-pink-s" id="quote3">
				<div class="container">
					<div class="col-xs-12 col-md-10 col-md-offset-1">
						<p class="h4 spaced arapey black m-small-bottom"><span class="quote-icon"><img src="./img/pictos/quote_white_left.png"></span>La 
						chose importante à garder en tête est qu'il ne faut jamais attendre une minute pour commencer à changer le 
						monde<span class="quote-icon"><img src="./img/pictos/quote_white_right.png"></span></p>
						<p class="h4 lobster white"><i class="fz09">- Anne Frank -</i></p>
					</div>
				</div>
			</div>
			<div class="section p-medium publications" id="publications">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-5">
							<h2><span class="fz09"><span class="pink">Télécharger</span><br><span class="relative">nos livres 
							blancs<img class="cut-slash hidden-xs" src="./img/pictos/cut_slash_right.png"></span></h2>
							<p class="p-small m-small"><span class="hyphen"></span></p>
						</div>
						<div class="col-xs-12 col-md-7 text-center">
							<div class="row">
								<div class="col-xs-12 col-sm-4">
									<p class="black uppercase fz08 bold montserrat">Big<br>Data</p>
									<p class="text-center p-small m-small-top"><span class="hyphen"></span></p>
									<p class=" uppercase bold montserrat">
										<a class="fz08 black" href="mailto:frederique.agnes@citizenrepublic.com?subject=Demande%20de%20livre%20blanc%20:%20Big%20Data&amp;body=Merci%20de%20m’adresser%20votre%20livre%20blanc%20-%20Big%20Data" 
										onclick="__gaTracker('send', 'event', 'mailto', 'Big Data');">
										<img src="./img/pictos/download.png"><br><br>
										Télécharger
										</a>
									</p>
										<hr class="visible-xs-block m-medium-bottom" />
								</div>
								<div class="col-xs-12 col-sm-4">
									<p class="black uppercase fz08 bold montserrat">Les enjeux<br>de la parité</p>
									<p class="text-center p-small m-small-top"><span class="hyphen"></span></p>
									<p class=" uppercase bold montserrat">
										<a class="fz08 black" href="mailto:frederique.agnes@citizenrepublic.com?subject=Demande%20de%20livre%20blanc%20:%20Les%20enjeux%20de%20la%20parité&amp;body=Merci%20de%20m’adresser%20votre%20livre%20blanc%20-%20Les%20enjeux%20de%20la%20parité" 
										onclick="__gaTracker('send', 'event', 'mailto', 'Les enjeux de la parité');">
										<img src="./img/pictos/download.png"><br><br>
										Télécharger
										</a>
									</p>
									<hr class="visible-xs-block m-medium-bottom" />
								</div>
								<div class="col-xs-12 col-sm-4">
									<p class="black uppercase fz08 bold montserrat">Lutter contre<br>les stéréotypes</p>
									<p class="text-center p-small m-small-top"><span class="hyphen"></span></p>
									<p class=" uppercase bold montserrat">
										<a class="fz08 black" href="mailto:mailto:frederique.agnes@citizenrepublic.com?subject=Demande%20de%20livre%20blanc%20:%20Lutter%20contre%20les stéréotypes&amp;body=Merci%20de%20m’adresser%20votre%20livre%20blanc%20Lutter%20contre%20les%20stéréotypes." 
										onclick="__gaTracker('send', 'event', 'mailto', 'Lutter contre les stéréotypes.');">
										<img src="./img/pictos/download.png"><br><br>
										Télécharger
										</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="section p-medium bckgd-lightgrey-s black" id="publications">
				<div class="container">
					<div class="row m-medium-bottom">
						<div class="col-xs-12 col-sm-6 col-md-2 text-center">
							<img src="./img/pictures/mixite.jpg">	
						</div>
						<div class="col-xs-12 col-sm-6 col-md-2 text-center">
							<p class="black uppercase fz08 bold montserrat">Dossier presse</p>
							<p class="p-small m-small"><span class="hyphen"></span></p>
							<p class="uppercase bold montserrat">
								<a class="fz08 black" href="./docs/DP-Mixite-des-Metiers.pdf" 
								onclick="__gaTracker('send', 'event', 'download', 'Dossier Presse Mixite');">
								<img src="./img/pictos/download.png"><br><br>
								Télécharger
								</a>
							</p>
						</div>
						<div class="col-xs-12 col-md-8">
							<p>Citizen Republic est membre fondateur de la Fondation Égalité-Mixité sous égide de FACE (Fondation Agir 
							Contre l’Exclusion).</p>
							<p>Sous l’impulsion de Najat Vallaud-Belkacem, alors Ministre des Droits des Femmes, les 
							entreprises Axa, GDF SUEZ, Michelin et Orange créent la <span class="pink">«Fondation Égalité-Mixité»</span> sous égide de FACE en 
							juillet 2014 afin de développer l’égalité professionnelle et la mixité des métiers.</p>
							<p> Cette fondation thématique 
							a pour ambition de financer des actions favorisant la mixité des métiers ainsi que l’égalité professionnelle 
							entre les femmes et les hommes en entreprise.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-2 text-center">
							<hr class="visible-xs-block m-medium-bottom" />
							<img src="./img/pictures/womens_award.jpg">	
						</div>
						<div class="col-xs-12 col-sm-6 col-md-2 text-center">
							<p class="black uppercase fz08 bold montserrat">Extrait de la candidature</p>
							<p class="p-small m-small"><span class="hyphen"></span></p>
							<p class=" uppercase bold montserrat">
								<a class="fz08 black" href="mailto:mailto:frederique.agnes@citizenrepublic.com?subject=Demande%20de%20document%20:%20Extrait%20de%20la%20candidature%20aux%20Women%20s%20Awards%20de%20La%20Tribune&amp;body=Merci%20de%20m’adresser%20Extrait%20de%20candidature%20aux%20Women%20s%20Awards%20de%20La%20Tribune." 
								onclick="__gaTracker('send', 'event', 'mailto', 'frederique.agnes@citizenrepublic.com?subject=Demande%20de%20document%20:%20Extrait%20de%20la%20candidature%20aux%20Women%20s%20Awards%20de%20La%20Tribune&amp;body=Merci%20de%20m’adresser%20Extrait%20de%20candidature%20aux%20Women%20s%20Awards%20de%20La%20Tribune.');">
								<img src="./img/pictos/download.png"><br><br>
								Télécharger
								</a>
							</p>
						</div>
						<div class="col-xs-12 col-md-8">
							<p>Le jury des Women’s Awards de La Tribune a décerné le <span class="pink">« Prix coup de coeur du jury 2013 »</span> à Frédérique 
							Agnès, pour son parcours d’exception et son engagement citoyen.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="section p-medium bckgd-pink-s text-center" id="quote4">
				<div class="container">
					<div class="col-xs-12">
						<p class="h4 spaced arapey black m-small-bottom"><span class="quote-icon"><img src="./img/pictos/quote_white_left.png"></span>The biggest problem is 
						not to let people accept new ideas, but to let them forget the old 
						ones<span class="quote-icon"><img src="./img/pictos/quote_white_right.png"></span></p>
						<p class="h4 lobster white"><i class="fz09">- John Meynard Keynes -</i></p>
					</div>
				</div>
			</div>
			
			<!-- Modal -->
			<div class="modal fade modal-video" id="videoCZR" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content bckgd-black">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 			        <iframe class="p-small-top" title="YouTube video player" width="100%" height="300" src="" frameborder="0" allowfullscreen="" data-url="http://www.youtube.com/embed/is3xrAXycqo?rel=0"></iframe>
			      </div>
			    </div>
			  </div>
			</div>
			
		<?php include './fragments/footer.php'; ?>
	</body>
</html>
