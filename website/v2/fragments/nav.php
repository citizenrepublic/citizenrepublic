<nav class="navbar navbar-default navbar-fixed-top ">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
        <span class="sr-only">Afficher le menu</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">
      	<img src="./img/pictures/logo_citizenrepublic@2x.png">
      </a>
    </div>

    <div class="collapse navbar-collapse pull-right" id="main-menu">
      <ul class="nav navbar-nav uppercase">
        <li class="active"><a href="#home" class="softscroll">Accueil <span class="sr-only">(active)</span></a></li>
        <li><a href="#presentation" class="softscroll">Présentation</a></li>
        <li><a href="#references" class="softscroll">Références</a></li>
        <li><a href="#metiers" class="softscroll">Métiers</a></li>
        <li><a href="#equipe" class="softscroll">Équipe</a></li>
        <li><a href="#contact" class="softscroll">Contact</a></li>
        <li><a href="#publications" class="softscroll">Publications</a></li>
<!--         <li class="social fz18"><a href="#"><i class="fa fa-facebook"></i></a></li> -->
<!--         <li class="social fz18"><a href="#"><i class="fa fa-twitter"></i></a></li> -->
<!--         <li class="social fz18"><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>