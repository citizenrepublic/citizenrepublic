<div class="section" id="footer">
	<p class="text-center uppercase montserrat bold p-small fz08">
		2015 Tous droits réservés CITIZEN REPUBLIC - <a class="pink pointer softscroll" href="#footer" 
		type="button" data-toggle="collapse" data-target="#collapseLegalNotice"
		aria-expanded="false" aria-controls="collapseLegalNotice">Mentions Légales</a>
	</p>	
</div>
<div class="section collapse" id="collapseLegalNotice">
	<div class="container">
		<hr>
		<p class="text-center">
			Le site CitizenRepublic.com est édité par&nbsp;:<br><br>
			<span class="bold uppercase montserrat">Citizen Republic SAS</span><br>
			Société par Actions Simplifiée au capital de 20000€<br>
			171 Bis, Av Charles de Gaulle<br>
			92 200 Neuilly sur Seine<br>
			Tel 01 40 88 10 42<br>
			Présidente et Directrice de la Publication&nbsp;: Frédérique AGNES
		</p>
		<hr>
		<p class="text-center">
			Le site CitizenRepublic.com est hébergé par&nbsp;:<br><br>
			<span class="bold uppercase montserrat">OVH</span><br>
			SAS au capital de 10 000 000 €<br>
			RCS Roubaix – Tourcoing<br>
			424 761 419 00045<br>
			Siège social : 2 rue Kellermann – 59100 Roubaix – France.<br>
		</p>
		<hr>
		<p class="fz08 text-center">Ce site respecte le droit d’auteur. Tous les droits des auteurs des Œuvres 
			protégées reproduites et communiquées sur ce site, sont réservés. Sauf autorisation, toute 
			utilisation des Oeuvres autres que la reproduction et la consultation individuelles et privées sont interdites.
		</p>
	</div>
</div>
