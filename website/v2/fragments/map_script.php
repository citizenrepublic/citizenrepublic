<!-- 
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuFcRorU4v1aQeosQRayIVa9mxJViGpxg&sensor=false"></script>    
-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        
<script type="text/javascript">
// When the window has finished loading create our google map below
	google.maps.event.addDomListener(window, 'load', init);
        
	function init() {
		// Basic options for a simple Google Map
		// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
		var mapOptions = {
			// How zoomed in you want the map to start at (always required)
			zoom: 16,
			
			// Disable default UI
			disableDefaultUI: true,

			// The latitude and longitude to center the map (always required)
			center: new google.maps.LatLng(48.883362, 2.260609),

			// How you would like to style the map. 
			// This is where you would paste any style found on Snazzy Maps.
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":20}]},
				{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":30}]},
				{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":60}]},
				{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":60}]},
				{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"on"}]},
				{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},
				{"featureType":"poi","elementType":"labels.text","stylers":[{"visibility":"on"}]},
				{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
				{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":40}]},
				{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"on"}]},
				{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},
				{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},
				{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]},
				{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-50},{"lightness":30}]}]
		};

		// Get the HTML DOM element that will contain your map 
		// We are using a div with id="map" seen below in the <body>
		var mapElement = document.getElementById('map');

		// Create the Google Map using our element and options defined above
		var map = new google.maps.Map(mapElement, mapOptions);

		var image = 'img/pictos/pin.png';
		// Let's also add a marker while we're at it
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(48.883555,2.2628785),
			map: map,
			icon: image,
			title: 'Citizen Republic'
		});
		
	}
</script>